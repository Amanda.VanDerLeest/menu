from django.apps import AppConfig


class MenuRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "menu_rest"
